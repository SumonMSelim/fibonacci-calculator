<?php

namespace SumonMSelim\FibonacciCalculator\Tests;

use PHPUnit\Framework\TestCase;
use SumonMSelim\FibonacciCalculator\FibonacciCalculator;

class FibonacciCalculatorTest extends TestCase
{
    /** @test */
    public function it_calculates_0th_fibonacci()
    {
        $calculator = new FibonacciCalculator();
        $nThFibonacci = $calculator->getNumber(0);

        $this->assertEquals(0, $nThFibonacci);
    }

    /** @test */
    public function it_calculates_1st_fibonacci()
    {
        $calculator = new FibonacciCalculator();
        $nThFibonacci = $calculator->getNumber(1);

        $this->assertEquals(1, $nThFibonacci);
    }

    /** @test */
    public function it_calculates_3rd_fibonacci()
    {
        $calculator = new FibonacciCalculator();
        $nThFibonacci = $calculator->getNumber(3);

        $this->assertEquals(2, $nThFibonacci);
    }

    /** @test */
    public function it_calculates_10th_fibonacci()
    {
        $calculator = new FibonacciCalculator();
        $nThFibonacci = $calculator->getNumber(10);

        $this->assertEquals(55, $nThFibonacci);
    }
}
