# N-th Fibonacci number calculator

[![pipeline status](https://gitlab.com/SumonMSelim/fibonacci-calculator/badges/master/pipeline.svg)](https://gitlab.com/SumonMSelim/fibonacci-calculator/-/commits/master)
[![coverage report](https://gitlab.com/SumonMSelim/fibonacci-calculator/badges/master/coverage.svg)](https://gitlab.com/SumonMSelim/fibonacci-calculator/-/commits/master)

A PHP library which calculates the [N-th Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number).

## Usage

``` php
use SumonMSelim\FibonacciCalculator\FibonacciCalculator;

$calculator = new FibonacciCalculator();
$nThFibonacci = $calculator->getNumber(10);
```

## Development

- Start development environment

``` bash
./dev start
```

- Stop development environment

``` bash
./dev stop
```

- Restart development environment

``` bash
./dev restart
```

- Install composer dependencies

``` bash
./dev composer install
```

## Testing and others

- Run tests (uses PHPUnit)

``` bash
./dev composer test
```

- Apply PHP code style fix (uses PHP CS Fixer)

``` bash
./dev composer cs-fix
```

- Perform analysis on code (uses PHPStan)

``` bash
./dev composer analyse
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email [sumonmselim@gmail.com](mailto:sumonmselim@gmail.com).

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
