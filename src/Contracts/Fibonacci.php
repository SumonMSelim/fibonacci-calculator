<?php

namespace SumonMSelim\FibonacciCalculator\Contracts;

interface Fibonacci
{
    /**
     * Calculates n-th fibonacci number.
     *
     * @param int $n
     * @return int
     */
    public function getNumber(int $n): int;
}
