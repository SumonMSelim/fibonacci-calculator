<?php

namespace SumonMSelim\FibonacciCalculator;

use SumonMSelim\FibonacciCalculator\Contracts\Fibonacci;

class FibonacciCalculator implements Fibonacci
{
    /**
     * Calculates n-th fibonacci number.
     *
     * @param int $n
     * @return int
     */
    public function getNumber(int $n): int
    {
        if ($n <= 1) {
            return $n;
        }

        $n2 = 0;
        $n1 = 1;

        for ($i = 2; $i < $n; $i++) {
            $_n2 = $n2;
            $n2 = $n1;
            $n1 = ($n1 + $_n2);
        }

        return $n2 + $n1;
    }
}
