# Changelog

All notable changes to this library will be documented in this file.

## 1.0.0 - 2020-07-28

- initial release
